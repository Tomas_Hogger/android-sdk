package com.example.macbook.buttonactivity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

     TextView textView;
     Button button;
     boolean colorOfTextView = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView);
        button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                if(colorOfTextView) {
                    textView.setBackgroundColor(Color.WHITE);
                    colorOfTextView = false;
                } else {
                    textView.setBackgroundColor(Color.RED);
                    colorOfTextView = true;
                }

            }
        });
    }
}

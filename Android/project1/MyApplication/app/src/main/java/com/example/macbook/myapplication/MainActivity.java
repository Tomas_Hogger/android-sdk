package com.example.macbook.myapplication;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {


    private static final int TICK_DELAY_MILLIS = 250;

    private final Handler handler = new Handler(Looper.getMainLooper());
    private final SimpleDateFormat mDateFormat = new SimpleDateFormat("dd:MM:yyyy HH:mm:ss", Locale.US);
    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            textView.setText(mDateFormat.format(Calendar.getInstance().getTime()));
            handler.postDelayed(runnable, TICK_DELAY_MILLIS);
        }
    };

    private TextView textView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textViewer);
        runnable.run();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        handler.removeCallbacks(runnable);
        textView = null;
    }
}
